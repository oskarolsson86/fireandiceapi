package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class SwornMembers {

    // Method that allows the user to display all sworn members of previous characters house.
    public void printSwornMembers(String urlToHouse) {
        try {

            URL url = new URL(urlToHouse);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("accept", "application/json");
            con.getResponseCode();

            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                content.append(inputLine);
            }
            br.close();

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK){

                JSONObject jsonObject = new JSONObject(content.toString());
                JSONArray swornMembers = jsonObject.getJSONArray("swornMembers");

                for (int i = 0; i < swornMembers.length(); i++) {
                    URL urlToEverySwornMember = new URL(swornMembers.getString(i));

                    HttpURLConnection connection =  (HttpURLConnection) urlToEverySwornMember.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("accept", "application/json");

                    InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String input;
                    StringBuffer cont = new StringBuffer();

                    while ((input = bufferedReader.readLine()) != null) {
                        cont.append(input);
                    }
                    br.close();
                    JSONObject json = new JSONObject(cont.toString());
                    String name = json.getString("name");
                    System.out.println("Name: " + name);

                }

            }
        } catch (ProtocolException protocolException) {
            protocolException.printStackTrace();
        } catch (MalformedURLException malformedURLException) {
            malformedURLException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
