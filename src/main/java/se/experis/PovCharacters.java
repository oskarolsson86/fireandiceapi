package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class PovCharacters {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    // Method that displays all books (titles) from the publisher "Bantam Books", and also all the pov Characters in every book.
    public void printPovCharacters() {
        try {

            URL url = new URL("https://anapioficeandfire.com/api/books/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("accept", "application/json");
            con.getResponseCode();

            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                content.append(inputLine);
            }
            br.close();

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK){

                JSONArray jsonArr = new JSONArray(content.toString());

                // Initial loop through all books and find books with publisher "Bantam Books",
                // then loop through all pov characters in respective books.
                for (int i = 0; i < jsonArr.length(); i++) {
                    JSONObject jsonObj = jsonArr.getJSONObject(i);
                    String publisher = jsonObj.getString("publisher");
                    if (publisher.equals("Bantam Books")) {
                        String bookTitle = jsonObj.getString("name");
                        JSONArray povChars = jsonObj.getJSONArray("povCharacters");
                        String leftAlignFormat = "| %-20s |%n";

                        System.out.format("+----------------------+%n");
                        System.out.format(leftAlignFormat, ANSI_RED + bookTitle + ANSI_RESET);

                        for (int j = 0; j < povChars.length(); j++) {
                            URL urlToPovCharacters = new URL(povChars.getString(j));
                            HttpURLConnection newCon = (HttpURLConnection) urlToPovCharacters.openConnection();

                            newCon.setRequestMethod("GET");
                            newCon.setRequestProperty("accept", "application/json");
                            newCon.getResponseCode();

                            InputStreamReader newInputStreamReader = new InputStreamReader(newCon.getInputStream());
                            BufferedReader newBufferedReader = new BufferedReader(newInputStreamReader);
                            String newInputLine;
                            StringBuffer newContent = new StringBuffer();

                            while ((newInputLine = newBufferedReader.readLine()) != null) {
                                newContent.append(newInputLine);
                            }
                            br.close();

                            JSONObject charJson = new JSONObject(newContent.toString());
                            String charName = charJson.getString("name");

                            System.out.format(leftAlignFormat, charName);

                        }
                    }
                }


            }
        } catch (ProtocolException protocolException) {
            protocolException.printStackTrace();
        } catch (MalformedURLException malformedURLException) {
            malformedURLException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
