package se.experis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

public class Character {

    // Method that displays information about a character based on ID given from user.
    public String printCharacter(String id) {
        try {

            URL url = new URL("https://anapioficeandfire.com/api/characters/" + id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("accept", "application/json");
            con.getResponseCode();

            InputStreamReader isr = new InputStreamReader(con.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String inputLine;
            StringBuffer content = new StringBuffer();

            while ((inputLine = br.readLine()) != null) {
                content.append(inputLine);
            }
            br.close();

            if(con.getResponseCode() == HttpURLConnection.HTTP_OK){

                JSONObject jsonObject = new JSONObject(content.toString());
                System.out.println(jsonObject.toString(4));
                JSONArray allegiange = jsonObject.getJSONArray("allegiances");
                String urlToHouse = allegiange.getString(0);
                return urlToHouse;
            }
        } catch (ProtocolException protocolException) {
            protocolException.printStackTrace();
        } catch (MalformedURLException malformedURLException) {
            malformedURLException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }
}
