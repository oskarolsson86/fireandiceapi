package se.experis;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Character character = new Character();
        SwornMembers swornMembers = new SwornMembers();
        PovCharacters povCharacters = new PovCharacters();

        System.out.println("Search for a character by it's ID");
        Scanner scanner = new Scanner(System.in);
        String idOfCharacter = scanner.nextLine();
        if (validateInput(idOfCharacter)) {
            String allegiances = character.printCharacter(idOfCharacter);
            System.out.println("Do you want to show all sworn members? type y/n");
            Scanner swornScanner = new Scanner(System.in);
            String yesOrNo = swornScanner.next();

            if (yesOrNo.equals("y")) {
                swornMembers.printSwornMembers(allegiances);
            }
        } else {
            System.out.println("Invalid ID, you're only allowed to type in numbers");
        }

        povCharacters.printPovCharacters();

    }
    private static boolean validateInput(String id) {
        String regex = "[0-9]+";
        if (id.matches(regex)) {
            return true;
        } else {
            return false;
        }
    }
}
